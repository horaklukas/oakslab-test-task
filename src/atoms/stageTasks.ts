import { Stage, StageTasks } from "@constants/stages";
import { atomWithStorage } from "jotai/utils";

import { APP_NAME } from "@constants/app";
import { uuid } from "@utils/index";

const defaultStageTasks: StageTasks = {
  [Stage.Foundation]: [
    { id: uuid(), name: "Setup virtual office" },
    { id: uuid(), name: "Set mission and vission" },
    { id: uuid(), name: "Select business name" },
  ],
  [Stage.Discovery]: [
    { id: uuid(), name: "Create roadmap" },
    { id: uuid(), name: "Competitor analysis" },
  ],
  [Stage.Delivery]: [
    { id: uuid(), name: "Release marketing website" },
    { id: uuid(), name: "Release MVP" },
  ],
};

export const stageTasksAtom = atomWithStorage<StageTasks>(
  `${APP_NAME}.stage-tasks`,
  defaultStageTasks
);
