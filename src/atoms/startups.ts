import { atomWithStorage } from "jotai/utils";

import { Stage, type Task } from "@constants/stages";
import { APP_NAME } from "@constants/app";

export type ExtendedTask = Task & { completed: boolean };

export interface Startup {
  id: string;
  name: string;
  tasks: Record<Stage, ExtendedTask[]>;
}

export const startupsAtom = atomWithStorage<Startup[]>(
  `${APP_NAME}.startups`,
  []
);
