"use client";

import { Inter } from "@next/font/google";
import BackButton from "@ui/BackButton";
import { usePathname } from "next/navigation";

import * as styles from "./layout.css";

const inter = Inter({ subsets: ["latin"] });

import "./globals.css";

interface RootLayoutProps {
  children: React.ReactNode;
}

export default function RootLayout({ children }: RootLayoutProps) {
  const pathname = usePathname();

  return (
    <html lang="en">
      <head />
      <body>
        <main className={`${styles.main} ${inter.className}`}>
          <div className={styles.topBar}>
            {pathname && pathname !== "/" && <BackButton />}
          </div>
          {children}
        </main>
      </body>
    </html>
  );
}
