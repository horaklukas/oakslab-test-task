import { style } from "@vanilla-extract/css";

export const nav = style({
  display: "inline-flex",
  gap: "1rem",

  margin: "1rem 0",
});

export const emptyMessage = style({
  margin: "1rem 0",
});

export const startup = style({
  listStyleType: "none",

  marginTop: "0.5rem",
});
