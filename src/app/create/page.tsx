"use client";

import { FormEvent } from "react";
import { useAtomValue, useSetAtom } from "jotai";
import cloneDeepWith from "lodash/cloneDeepWith";
import { useRouter } from "next/navigation";

import Button from "@ui/Button";
import StagesTasksList from "@ui/StageTasksList";
import FormItem from "@ui/FormItem";
import { stageTasksAtom } from "@atoms/stageTasks";
import { startupsAtom } from "@atoms/startups";
import { Task } from "@constants/stages";
import { uuid } from "@utils/index";
import MainTitle from "@ui/MainTitle";

import * as styles from "./page.css";

function isTask(task: unknown): task is Task {
  return "id" in (task as Task) && "name" in (task as Task);
}

function addFlagToTasks(clonedElement: unknown) {
  if (isTask(clonedElement)) {
    return { ...clonedElement, completed: false };
  }
}

export default function Create() {
  const router = useRouter();
  const stageTasks = useAtomValue(stageTasksAtom);
  const setStartups = useSetAtom(startupsAtom);

  // Same as for stages configuration, we're not doing any validation  🤞
  const handleSubmit = (ev: FormEvent<HTMLFormElement>) => {
    const form = ev.currentTarget;

    ev.preventDefault();

    const name = (form.elements.namedItem("name") as HTMLInputElement).value;
    const id = uuid();

    setStartups((startups) =>
      startups.concat({
        id,
        name,
        tasks: cloneDeepWith(stageTasks, addFlagToTasks),
      })
    );

    form.reset();
    router.push(`/startup/${id}`);
  };

  return (
    <>
      <MainTitle>Build new startup</MainTitle>
      <p>Create a new startup to start tracking his progress</p>

      <form onSubmit={handleSubmit} className={styles.form}>
        <FormItem id="startup-name" label="Startup name">
          <input id="startup-name" name="name" />
        </FormItem>

        <Button type="submit">Create startup</Button>

        <p>
          <small>with following tasks👇</small>
        </p>
      </form>
      <StagesTasksList stageTasks={stageTasks} />
    </>
  );
}
