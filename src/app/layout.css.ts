import { style } from "@vanilla-extract/css";

export const topBar = style({
  textAlign: "left",
});

export const main = style({
  textAlign: "center",

  padding: "6rem",
  minHeight: "100vh",
});
