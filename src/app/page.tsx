"use client";

import Link from "next/link";
import { useAtomValue } from "jotai";

import { startupsAtom } from "@atoms/startups";
import ButtonLink from "@ui/ButtonLink";
import MainTitle from "@ui/MainTitle";

import * as styles from "./page.css";

export default function Home() {
  const startups = useAtomValue(startupsAtom);

  return (
    <>
      <MainTitle>Startup progress tracker</MainTitle>

      <nav className={styles.nav}>
        <ButtonLink href="/configure">Configure stages</ButtonLink>
        <ButtonLink href="/create">Add new startup</ButtonLink>
      </nav>

      <h2>Select your startup</h2>

      {startups.length === 0 ? (
        <p className={styles.emptyMessage}>
          There are currently no startups create a first one ☝️
        </p>
      ) : (
        <ul>
          {startups.map((startup) => (
            <li key={startup.id} className={styles.startup}>
              <Link href={`/startup/${startup.id}`}>🚀 {startup.name}</Link>
            </li>
          ))}
        </ul>
      )}
    </>
  );
}
