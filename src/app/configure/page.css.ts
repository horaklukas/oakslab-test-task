import { style } from "@vanilla-extract/css";

export const form = style({
  display: "inline-flex",
  flexDirection: "column",
  gap: "0.5rem",

  margin: "1rem",
});
