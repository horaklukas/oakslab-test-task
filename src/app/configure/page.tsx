"use client";

import type { FormEvent } from "react";
import { useAtom } from "jotai";

import { stageTasksAtom } from "@atoms/stageTasks";
import Button from "@ui/Button";
import StagesTasksList from "@ui/StageTasksList";
import { uuid } from "@utils/index";
import { Stage, stages, Task } from "@constants/stages";

import FormItem from "@ui/FormItem";
import MainTitle from "@ui/MainTitle";

import * as styles from "./page.css";

const ConfigurePage = () => {
  const [stageTasks, setStageTasks] = useAtom(stageTasksAtom);

  // For the purpose of the demo, we're optimists and don't need any validation 🤞
  const handleSubmit = (ev: FormEvent<HTMLFormElement>) => {
    const form = ev.currentTarget;

    ev.preventDefault();

    const stage = (form.elements.namedItem("stage") as HTMLSelectElement)
      .value as unknown as Stage;
    const name = (form.elements.namedItem("name") as HTMLInputElement).value;
    const id = uuid();

    setStageTasks((state) => ({
      ...state,
      [stage]: state[stage].concat({ id, name }),
    }));

    form.reset();
  };

  const handleDelete = (task: Task, stage: Stage) => {
    const confirmed = confirm(`Delete ${task.name}, are you sure?`);

    if (confirmed) {
      setStageTasks((state) => ({
        ...state,
        [stage]: state[stage].filter(({ id }) => id !== task.id),
      }));
    }
  };

  return (
    <>
      <MainTitle>Configuration</MainTitle>
      <p>You can manage task for particular startup stages here.</p>

      <form onSubmit={handleSubmit} className={styles.form}>
        <FormItem label="Stage" id="assigned-stage">
          <select name="stage" id="assigned-stage">
            {stages.map((stage) => (
              <option key={stage} value={stage}>
                {Stage[stage as Stage]}
              </option>
            ))}
          </select>
        </FormItem>

        <FormItem label="Task name" id="task-name">
          <input id="task-name" name="name" />
        </FormItem>

        <Button type="submit">Add task</Button>
      </form>

      <StagesTasksList
        stageTasks={stageTasks}
        renderTask={(task, stage) => (
          <>
            {task.name}
            <button
              title={`Delete ${task.name}`}
              onClick={() => handleDelete(task, stage)}
            >
              ❌
            </button>
          </>
        )}
      />
    </>
  );
};

export default ConfigurePage;
