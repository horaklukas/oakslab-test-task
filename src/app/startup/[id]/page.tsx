"use client";

import { useAtomValue } from "jotai";

import { startupsAtom } from "@atoms/startups";
import MainTitle from "@ui/MainTitle";
import StartupDetail from "@modules/startup/StartupDetail";

// TODO - add page props type
export default function StartupPage({ params: { id } }) {
  const startups = useAtomValue(startupsAtom);

  const startup = startups.find((startup) => startup.id === id);

  return (
    <>
      {startup ? (
        <>
          <MainTitle>{startup.name} progress</MainTitle>
          <StartupDetail detail={startup} />
        </>
      ) : (
        <p>We can not find your startup</p>
      )}
    </>
  );
}
