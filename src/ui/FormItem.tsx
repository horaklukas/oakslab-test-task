import type { ReactNode } from "react";
import * as styles from "./FormItem.css";

interface FormItemProps {
  label: string;
  id: string;
  children: ReactNode;
}

const FormItem = ({ label, id, children }: FormItemProps) => {
  return (
    <div className={styles.formItem}>
      {label && <label htmlFor={id}>{label}</label>}
      {children}
    </div>
  );
};

export default FormItem;
