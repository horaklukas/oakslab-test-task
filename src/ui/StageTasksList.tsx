import { ReactNode } from "react";
import { Stage, stages, Task, type StageTasks } from "@constants/stages";

import * as styles from "./StageTasksList.css";

interface StagesTasksListProps {
  stageTasks: StageTasks;
  renderTask?: (task: Task, stage: Stage) => ReactNode;
}

const defaultTaskRender = (task: Task) => <>{task.name}</>;

const StagesTasksList = ({
  stageTasks,
  renderTask = defaultTaskRender,
}: StagesTasksListProps) => {
  return (
    <div className={styles.stagesList}>
      {stages.map((stage) => (
        <h2 key={stage}>{Stage[stage]}</h2>
      ))}
      {stages.map((stage) => {
        const tasks = stageTasks[stage];

        return (
          <ul key={stage} className={styles.tasksList}>
            {tasks.map((task) => (
              <li key={task.id} className={styles.task}>
                {renderTask(task, stage)}
              </li>
            ))}
          </ul>
        );
      })}
    </div>
  );
};

export default StagesTasksList;
