import type { ButtonHTMLAttributes } from "react";

import * as styles from "./Button.css";

interface ButtonProps
  extends Omit<ButtonHTMLAttributes<HTMLButtonElement>, "className"> {}

const Button = (props: ButtonProps) => {
  return <button {...props} className={styles.button} />;
};

export default Button;
