import { style } from "@vanilla-extract/css";

export const button = style({
  marginRight: "1rem",

  fontSize: "0.9rem",
});
