import { style } from "@vanilla-extract/css";

export const formItem = style({
  display: "flex",
  gap: "1rem",
  justifyContent: "space-between",
});
