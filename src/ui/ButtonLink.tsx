import Link, { type LinkProps } from "next/link";

import * as styles from "./Button.css";

interface ButtonLinkProps extends Omit<LinkProps, "className"> {}

const ButtonLink = (props: ButtonLinkProps) => {
  return <Link {...props} className={styles.button} />;
};

export default ButtonLink;
