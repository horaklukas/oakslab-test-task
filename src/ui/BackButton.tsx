import Link from "next/link";
import * as styles from "./BackButton.css";

interface BackButtonProps {
  target?: string;
}

const BackButton = ({ target = "/" }: BackButtonProps) => {
  return (
    <Link href={target} className={styles.button}>
      ⬅️ Back
    </Link>
  );
};

export default BackButton;
