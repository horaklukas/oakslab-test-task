import { style } from "@vanilla-extract/css";

export const button = style({
  padding: "0.25rem 0.5rem",

  borderRadius: "0.25rem",

  background: "white",
  color: "black",

  selectors: {
    "&[type=submit]": {
      marginTop: "1rem",

      alignSelf: "center",
    },
  },
});
