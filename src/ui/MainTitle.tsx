import type { PropsWithChildren } from "react";

import * as styles from "./MainTitle.css";

const MainTitle = ({ children }: PropsWithChildren<{}>) => {
  return <h1 className={styles.title}>{children}</h1>;
};

export default MainTitle;
