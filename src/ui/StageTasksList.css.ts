import { style } from "@vanilla-extract/css";

export const stagesList = style({
  display: "grid",
  gridTemplateColumns: "repeat(3, 1fr)",

  marginTop: "1rem",
  paddingTop: "1rem",

  borderTop: "1px solid dimgrey",
});

export const tasksList = style({
  margin: "0.5rem 0 1rem 0",

  listStyleType: "none",
});

export const task = style({
  display: "flex",
  justifyContent: "center",

  gap: "2rem",
});
