export enum Stage {
  Foundation,
  Discovery,
  Delivery,
}

export const stages = [Stage.Foundation, Stage.Discovery, Stage.Delivery];

export interface Task {
  id: string;
  name: string;
}

export type StageTasks = Record<Stage, Task[]>;
