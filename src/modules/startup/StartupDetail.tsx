import type { Startup } from "@atoms/startups";
import { Stage, stages } from "@constants/stages";

import StageDetail from "./StageDetail";

import SuccessMessage from "./SuccessMessage";
import { useStartupState } from "./useStartupState";
import { useTaskComplete } from "./useTaskComplete";

interface StartupDetailProps {
  detail: Startup;
}

const StartupDetail = ({ detail }: StartupDetailProps) => {
  const { stageCompleted, stageEnabled, allStagesCompleted } = useStartupState(
    detail.tasks
  );
  const handleTaskCompleted = useTaskComplete();

  return (
    <>
      {allStagesCompleted && <SuccessMessage />}

      {stages.map((stage) => {
        const stageTasks = detail.tasks[stage];

        return (
          <StageDetail
            key={stage}
            name={Stage[stage]}
            tasks={stageTasks}
            disabled={allStagesCompleted || !stageEnabled[stage]}
            completed={stageCompleted[stage]}
            onTaskComplete={(taskId, completed) =>
              handleTaskCompleted(detail.id, stage, taskId, completed)
            }
          />
        );
      })}
    </>
  );
};

export default StartupDetail;
