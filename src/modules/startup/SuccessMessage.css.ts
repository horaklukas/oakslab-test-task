import { style } from "@vanilla-extract/css";

export const quote = style({
  margin: "1rem auto",

  fontStyle: "italic",

  maxWidth: "30rem",
});
