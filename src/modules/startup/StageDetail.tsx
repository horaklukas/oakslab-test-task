import { ExtendedTask } from "@atoms/startups";
import type { Task } from "@constants/stages";

import * as styles from "./StageDetail.css";

interface StageDetailProps {
  name: string;
  tasks: ExtendedTask[];
  disabled: boolean;
  completed: boolean;
  onTaskComplete: (id: Task["id"], completed: boolean) => void;
}

const StageDetail = ({
  name,
  disabled,
  completed,
  tasks,
  onTaskComplete,
}: StageDetailProps) => {
  return (
    <fieldset disabled={disabled} className={styles.stage}>
      <legend className={styles.title}>
        {name}
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="48"
          height="48"
          fill="currentColor"
          viewBox="0 0 256 256"
          className={styles.completedIcon}
        >
          <rect width="256" height="256" fill="none"></rect>
          <polyline
            points="216 72 104 184 48 128"
            fill="none"
            stroke={completed ? "currentColor" : "none"}
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="32"
          ></polyline>
        </svg>
      </legend>

      <ul>
        {tasks.map((task) => (
          <li key={task.id} className={styles.task}>
            <input
              type="checkbox"
              defaultChecked={task.completed}
              id={task.id}
              onClick={(e) => onTaskComplete(task.id, e.currentTarget.checked)}
            />{" "}
            <label htmlFor={task.id}>{task.name}</label>
          </li>
        ))}
      </ul>
    </fieldset>
  );
};

export default StageDetail;
