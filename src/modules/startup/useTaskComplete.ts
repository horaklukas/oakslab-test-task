import { useSetAtom } from "jotai";

import { startupsAtom } from "@atoms/startups";
import { Stage } from "@constants/stages";

export const useTaskComplete = () => {
  const updateStartups = useSetAtom(startupsAtom);

  return function completeTask(
    startupId: string,
    stage: Stage,
    taskId: string,
    completed: boolean
  ) {
    updateStartups((startups) => {
      const index = startups.findIndex((startup) => startup.id === startupId);
      const nextStartups = [...startups];

      if (index > -1) {
        const startup = startups[index];
        const stageTasks = startup.tasks[stage];
        const nextStageTasks = [...stageTasks];

        const taskIndex = stageTasks.findIndex((task) => task.id === taskId);

        if (taskIndex > -1) {
          nextStageTasks[taskIndex] = {
            ...nextStageTasks[taskIndex],
            completed,
          };

          nextStartups[index] = {
            ...startup,
            tasks: {
              ...startup.tasks,
              [stage]: nextStageTasks,
            },
          };
        }

        return nextStartups;
      }

      return startups;
    });
  };
};
