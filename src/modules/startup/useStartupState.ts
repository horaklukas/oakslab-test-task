import { ExtendedTask } from "@atoms/startups";
import { Stage, stages } from "@constants/stages";
import { useMemo } from "react";

export const useStartupState = (tasks: Record<Stage, ExtendedTask[]>) => {
  const stageCompleted: Record<Stage, boolean> = useMemo(() => {
    const state = {} as Record<Stage, boolean>;

    for (const stage of stages) {
      state[stage] = tasks[stage].every((task) => task.completed);
    }

    return state;
  }, [tasks]);

  const stageEnabled: Record<Stage, boolean> = useMemo(() => {
    const state = {} as Record<Stage, boolean>;
    let prevStage = undefined;

    for (const stage of stages) {
      state[stage] = prevStage === undefined || stageCompleted[prevStage];

      prevStage = stage;
    }

    return state;
  }, [stageCompleted]);

  const allStagesCompleted = useMemo(
    () => stages.every((stage) => stageCompleted[stage]),
    [stageCompleted]
  );

  return { stageCompleted, stageEnabled, allStagesCompleted };
};
