import { style } from "@vanilla-extract/css";

export const stage = style({
  display: "inline-flex",

  paddingTop: "1rem",
  margin: "1rem",

  textAlign: "left",

  border: 0,

  counterIncrement: "stage-counter",

  ":disabled": {
    opacity: 0.8,
  },
});

export const completedIcon = style({
  marginLeft: "1.5rem",
});

export const title = style({
  fontSize: "1.5rem",
  fontWeight: "bold",

  display: "inline-flex",
  height: 50,

  alignItems: "center",
  justifyContent: "space-between",

  "::before": {
    content: "counter(stage-counter)",

    width: "1.5rem",
    height: "1.5rem",

    display: "inline-flex",
    alignItems: "center",
    justifyContent: "center",

    padding: "0.25rem",
    marginRight: "0.5rem",

    borderRadius: "50%",

    background: "white",
    color: "black",

    fontSize: "0.9emrem",
  },
});

export const task = style({
  margin: "0.5rem 0",

  listStyleType: "none",
  textAlign: "left",
});
