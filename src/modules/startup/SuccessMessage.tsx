import { useEffect, useState } from "react";

import * as styles from "./SuccessMessage.css";

const SuccessMessage = () => {
  const [fact, setFact] = useState<string>();

  useEffect(() => {
    fetch("https://uselessfacts.jsph.pl/random.json")
      .then((response) => response.json())
      .then(({ text }) => setFact(text));
  }, []);

  return (
    <>
      <p>Congratulations, your startup is ready to roll! 🎉</p>
      {fact && <blockquote className={styles.quote}>{fact}</blockquote>}
    </>
  );
};

export default SuccessMessage;
