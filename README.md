# Oak's lab test task

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Technologies

This project uses

[`vanilla extract`](https://vanilla-extract.style/) for styling

[`jotai`](https://jotai.org/) for stage managemnt

[`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Workarounds

[Support of vanilla extract in Next 13](https://gist.github.com/bizarre/825cab8224c28e93ff1a3933642f9271)
